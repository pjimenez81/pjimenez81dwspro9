<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Persona;

class PersonaController extends Controller
{
    public function index() {
$personas = Persona::get();
return view('personas.index')->with('personas', $personas);
}
    
public function create() {
return view('personas.create');
}
    
public function store(Request $request) {
$persona = new Persona;
$max = Persona::get()->max('id');
$persona->id = $max + 1;
$persona->nombre = $request->input('nombre');
$persona->save();
return redirect()->route('personas.index');
}
    
    public function edit($id) {
$persona = Persona::find($id);
return view('personas.edit')->with('persona', $persona);
}
    
public function update(Request $request, $id) {
$persona = Persona::find($id);
$persona->id = $id;
$persona->nombre = $request->input('nombre');
$persona->save();
return redirect()->route('personas.index');
}
    
public function destroy($id) {
Persona::destroy($id);
return redirect()->route('personas.index');
}
   
public function show($id) {
   $persona = Persona::find($id);
   return View::make('persona.show')->with('persona', $persona);
}
    
}