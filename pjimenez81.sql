-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2017 a las 16:31:43
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pjimenez81`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`) VALUES
(1, 'Pablo'),
(2, 'Luis'),
(3, 'Pablo'),
(4, 'Luis'),
(5, 'Pablo'),
(6, 'Luis'),
(7, 'Pablo'),
(8, 'Luis'),
(9, 'Pablo'),
(10, 'Luis'),
(11, 'Pablo'),
(12, 'Luis'),
(13, 'Pablo'),
(14, 'Luis'),
(15, 'Pablo'),
(16, 'Luis'),
(17, 'ANDRES'),
(18, 'Pablo'),
(19, 'Luis'),
(20, 'Manuel'),
(21, 'Luisa Fernanda'),
(22, 'Jacinto'),
(23, 'Pablo'),
(24, 'Luis'),
(25, 'PABLO78178728'),
(26, 'Pablo'),
(27, 'Luis'),
(28, 'PACO'),
(29, 'JOSE MIGUEL'),
(30, 'Pablo'),
(31, 'Luis'),
(32, 'paco'),
(33, 'paco8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(10) UNSIGNED NOT NULL,
  `numero` varchar(25) DEFAULT NULL,
  `idpersona` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `numero`, `idpersona`) VALUES
(1, '666112233', 1),
(2, '666998877', 2),
(3, '666112233', 1),
(4, '666998877', 2),
(5, '666112233', 1),
(6, '666998877', 2),
(7, '607074679', 3),
(8, '666112233', 1),
(9, '666998877', 2),
(10, '666112233', 1),
(11, '666998877', 2),
(12, '666112233', 1),
(13, '666998877', 2),
(14, '666112233', 1),
(15, '666998877', 2),
(16, '666112233', 1),
(17, '666998877', 2),
(18, '444444444', 17),
(19, '666112233', 1),
(20, '666998877', 2),
(21, '999887766', 21),
(22, '666112233', 1),
(23, '666998877', 2),
(24, '999887766', 17),
(25, '666112233', 1),
(26, '666998877', 2),
(27, '999112233', 28),
(28, '666112233', 1),
(29, '666998877', 2),
(30, '1322', 32);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpersona` (`idpersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `telefonos_ibfk_1` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
