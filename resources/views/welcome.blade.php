<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Agenda de Teléfonos</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">AGENDA TELEFÓNICA</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Inicio</a></li>
                    <li><a href="{{ url('/personas') }}">Crear Personas</a></li>

                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>

    <div class="container">
        <br>
        <br>
        <br>
        <h1>PROYECTO LARAVEL. DWS 2016/2017</h1>
        <h4>PABLO JIMÉNEZ NOTARIO</h4>
        <ol>
            <li><a href="http://www.pjimenez81dwspro9.esy.es/Documentaci%c3%b3n_proyecto.pdf">DOCUMENTACIÓN</a></li>
            <li><a href="https://bitbucket.org/pjimenez81/pjimenez81dwspro9">ENLACE AL PROYECTO EN BITBUCKET</a></li>
            <li>DESPLIEGUE DE LA APLICACIÓN
                <br>
                <ul>
                    <li><strong>CLONAR EL PROYECTO:</strong> git clone https://bitbucket.org/pjimenez81/pjimenez81dwspro9</li>
                    <li><p><strong>DAR PERMISOS:</strong> Es necesario que el directorio en el que clonamos el proyecto tenga permisos de escritura.</p></li>
                    <li><strong>INSTALAR DEPENDENCIAS:</strong>composer install</li>
                    <li><strong>FICHERO .ENV:</strong><p>Copiar el fichero .env.example renombrándolo a .env y configurar los apartados DB_DATABASE, DB_USERNAME y DB_PASSWORD con el nombre de nuestra base de datos, usuario y contraseña.</p></li>
                    <li><strong>CREAR NUEVA API-KEY:</strong> php artisan key:generate</li>
                    <li><strong>CREAR BASE DE DATOS: </strong> Es necesario crear la base de datos con el mismo nombre que le dimos en el fichero .env.</li>
                    <li><strong>MIGRAR: </strong> php artisan migrate</li>
                    <li><strong>PROYECTO CLONADO Y LISTO PARA FUNCIONAR</strong</li>
                </ul>
            </li>

        </ol>
    </div>
    <!-- /.container -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>