@extends('personas')
@section('content')

<h1 class="text-primary">Añadir Personas</h1>
<a class="btn btn-success pull-right" href="{{ url('/personas/create') }}" role="button">Nueva persona</a>
<table class="table table-bordered table-striped table-hover" id="MyTable">
<thead>
<tr>
<th class="text-center">ID</th>
<th class="text-center">Nombre</th>
<th class="text-center">Acciones</th>
</tr>
</thead>
<tbody>
@foreach($personas as $persona)
<tr>
<td class="text-center">{{ $persona->id }}</td>
<td class="text-center">{{ $persona->nombre }}</td>

{!! Form::open(['route' => ['personas.destroy', $persona->id], 'method' => 'DELETE']) !!}
<form action="{{route('personas.destroy', $persona->id)}}" method ="'DELETE'">
<td class="text-center">

<button type="submit" class="btn btn-danger btn-xs">
<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
</button>
<a href="{{ url('/personas/'.$persona->id.'/edit') }}" class="btn btn-info btn-xs">
<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
</a>
</td>
</form>
{!! Form::close() !!}
</tr>
@endforeach
</tbody>
<tfoot>
<!--<tr>
<th class="text-center">ID</th>
<th class="text-center">Nombre</th>
<th class="text-center">Acciones</th>
</tr>-->
</tfoot>
</table>
<a class="btn btn-primary pull-right" href="{{ url('/') }}" role="button">Volver</a>
@endsection