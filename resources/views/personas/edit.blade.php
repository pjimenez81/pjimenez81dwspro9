@extends('personas')
@section('content')
<h4 class="text-center">Editar persona: {{ $persona->nombre }}</h4>
<!--<form action="{{route('personas.update', $persona->id)}}" method ="'DELETE'">-->
{!! Form::model($persona, [ 'route' => ['personas.update', $persona], 'method' => 'PUT']) !!}
<!--</form>-->
@include('personas.partials.fields')
<button type="submit" class="btn btn-success btn-block">Guardar cambios</button>
{!! Form::close() !!}