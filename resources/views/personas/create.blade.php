@extends('personas')
@section('content')
<!--<form action="{{route('personas.store')}}" method ="POST">-->
{!! Form::open(['route' => 'personas.store', 'method' => 'POST']) !!}
<!--</form>-->
@include('personas.partials.fields')
<button type="submit" class="btn btn-success btn-block">Guardar</button>
{!! Form::close() !!}